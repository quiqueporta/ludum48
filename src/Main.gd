extends Node2D


var player
var current_player
var light

func _ready():
	player = load("res://Player.tscn")
	light = load("res://Light.tscn")
	current_player = get_node("Player")	

func _new_player():
	current_player = player.instance()
	current_player.position = Vector2(40.0, 140.0)
	current_player.activate_camera()
	current_player.connect("player_die", self, "_on_Player_player_die")
	current_player.connect("oxygen_reduced", self, "_on_Player_oxygen_reduced")
	return current_player

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.get_action_strength("light"):
		var new_light = light.instance()
		new_light.position = current_player.position
		self.add_child(new_light)


func _on_Player_player_die():
	self.add_child(_new_player())
	get_node("CanvasLayer/OxygenBar").value = 100

func _on_Player_oxygen_reduced(current_value):
	get_node("CanvasLayer/OxygenBar").value = current_value
