extends KinematicBody2D


signal player_die()
signal oxygen_reduced(current_value)

export var speed: = Vector2(40.0, 35.0)
export var gravity: = 18.0
export var oxygen: = 100

var velocity = Vector2.ZERO

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta: float) -> void:
	var direction: = get_direction()
	velocity = calculate_move_velocity(
		velocity,
		direction,
		speed
	)
	velocity = move_and_slide(velocity, Vector2.UP)

func get_direction() -> Vector2:
	return Vector2(
		Input.get_action_strength("move_right") - Input.get_action_strength("move_left"),
		-1.0 if Input.get_action_strength("jump") and is_on_floor() else 1.0
	)

func calculate_move_velocity(
	linear_velocity: Vector2,
	direction: Vector2,
	speed: Vector2
) -> Vector2:

	var new_velocity: = linear_velocity
	new_velocity.x = speed.x * direction.x
	new_velocity.y += gravity * get_physics_process_delta_time()

	if direction.y == -1.0:
		new_velocity.y = speed.y * direction.y

	return new_velocity


func activate_camera():
	get_node("Camera2D").current = true


func _on_Timer_timeout():
	_lower_light_intensity()
	_reduce_oxigen()


func _lower_light_intensity():
	var current_light_energy = get_node("Light").energy
	current_light_energy -= 0.01

	if current_light_energy < 0:
		current_light_energy = 0

	get_node("Light").energy = current_light_energy

func _reduce_oxigen():
	oxygen -= 5
	if oxygen <= 0:
		queue_free()
		emit_signal("player_die")
	else:
		emit_signal("oxygen_reduced", oxygen)
